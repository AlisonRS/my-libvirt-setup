#!/bin/bash

systemctl set-property --runtime -- user.slice AllowedCPUs=0,1,2,3,16,17,18,19
systemctl set-property --runtime -- system.slice AllowedCPUs=0,1,2,3,16,17,18,19
systemctl set-property --runtime -- init.scope AllowedCPUs=0,1,2,3,16,17,18,19