#!/bin/bash

source "/etc/libvirt/hooks/kvm.conf"

modprobe vfio
modprobe vfio_iommu_type1
modprobe vfio_pci

virsh nodedev-detach $VIRSH_GPU_VIDEO
virsh nodedev-detach $VIRSH_GPU_AUDIO
virsh nodedev-detach $VIRSH_GPU_USB
virsh nodedev-detach $VIRSH_GPU_SERIAL