#!/bin/bash

source "/etc/libvirt/hooks/kvm.conf"

virsh nodedev-reattach $VIRSH_GPU_VIDEO
virsh nodedev-reattach $VIRSH_GPU_AUDIO
virsh nodedev-reattach $VIRSH_GPU_USB
virsh nodedev-reattach $VIRSH_GPU_SERIAL

modprobe -r vfio_pci
modprobe -r vfio_iommu_type1
modprobe -r vfio